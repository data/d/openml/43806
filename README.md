# OpenML dataset: Rainfall-in-Kerala-1901-2017

https://www.openml.org/d/43806

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Kerala is a state in South India known for its beautiful beaches and backwaters. Also known as 'God's own country' it was named one of the 10 paradises of the world by National Geographic. 
Due to climate change, Kerala has been affected by unprecedented rainfall/floods in recent years. This dataset was obtained from Indian government's open data initiative (see link below) to understand the trends. 
Content
This raw dataset has the monthly and annual rainfall in millimeters (mms) from the year 1901 to 2017.
Acknowledgements
I want to thank Govt. of India for opening up this data for further analysis.
Inspiration
Any interesting observations from the past? Can we forecast what the next few years might look like?
Reference source
https://data.gov.in/resources/sub-divisional-monthly-rainfall-1901-2017/api

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43806) of an [OpenML dataset](https://www.openml.org/d/43806). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43806/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43806/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43806/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

